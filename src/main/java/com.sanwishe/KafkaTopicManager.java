package com.sanwishe;

import org.apache.commons.cli.*;

import java.util.Optional;

public class KafkaTopicManager {
    private static boolean isTopicNotExists = true;

    public static void main(String[] args) {
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        options.addRequiredOption("z", "zookeeper", true, "zookeeper information of kafka");
        options.addRequiredOption("t", "topic", true, "kafka topic to be created");
        options.addOption("p", "partition", true, "number of partitions to be created, default value : 1.");
        options.addOption("r", "replication", true, "number of replication to be created, default value: 1.");
        options.addOption("h", "help", false, "Print this usage information");

        // Parse the program arguments
        CommandLine commandLine;
        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println("use -h for help");
            System.exit(0);
            return;
        }

        if (commandLine.hasOption('h') || commandLine.getOptions().length == 0) {
            System.out.println("usage : java -jar kafka-manager.jar -z [zookeeper] -t [topic] -p [partition] -r [replication]");
            System.out.println("\tpartition and replication are optional, both default value is 1.");
            System.out.println("example usage :");
            System.out.println("java -jar kafka-topic-manager.jar -z \"127.0.0.1:2181\" -t \"test\"");
            System.exit(0);
        }

        if (commandLine.hasOption('z') && commandLine.hasOption('t')) {
            String kafkaZk = commandLine.getOptionValue('z');
            String topic = commandLine.getOptionValue('t');
            Optional<String> partition = Optional.ofNullable(commandLine.getOptionValue('p'));
            Optional<String> replication = Optional.ofNullable(commandLine.getOptionValue('r'));

            System.out.printf("\nkafka zk information : %s", kafkaZk);

            System.out.printf("\ntopic : %s", topic);

            System.out.printf("\npartition : %d ", Integer.parseInt(partition.orElse("1")));

            System.out.printf("\nreplication : %d \n", Integer.parseInt(replication.orElse("1")));

            TopicCreator creator = new TopicCreator(kafkaZk);

            while (true) {
                try {
                    if (isTopicNotExists) {
                        isTopicNotExists = creator.isTopicNotExists(topic);
                        if (isTopicNotExists) {
                            creator.createTopic(topic, Integer.parseInt(partition.orElse("1")), Integer.parseInt(replication.orElse("1")));
                            continue;
                        } else {
                            System.out.printf("\ntopic %s create successfully.\n", topic);
                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
